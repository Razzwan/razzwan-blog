$(document).ready(function($){
    /*$("a.item").click(function(){
     $(".item").removeClass("active");
     $(this).addClass("active");
     });*/

    $('.accordion').accordion();

    $("div.content").click(function(){
        $(".content").removeClass("active");
        $(this).addClass("active");
    });

    // ready event
    var semanticMenuReady = function() {

        // selector cache
        var
            $dropdownItem = $('.ui.dropdown.item'),
            $popupItem    = $('.browse.item'),
            $menuItem     = $('.menu a.item, .menu .link.item').not($dropdownItem),
            $dropdown     = $('.ui.dropdown'),
        // alias
            handler = {

                activate: function() {
                    if(!$(this).hasClass('dropdown browse')) {
                        $(this)
                            .addClass('active')
                            .closest('.ui.menu')
                            .find('.item')
                            .not($(this))
                            .removeClass('active')
                        ;
                    }
                }

            }
            ;

        $dropdown
            .dropdown({
                on: 'hover'
            })
        ;

        $('.ui.search')
            .search({
                type: 'category',
                apiSettings: {
                    action: 'categorySearch'
                }
            })
        ;

        $('.browse.item')
            .popup({
                popup     : '.admission.popup',
                hoverable : true,
                position  : 'bottom left',
                delay     : {
                    show: 300,
                    hide: 800
                }
            })
        ;

        $popupItem
            .popup({
                inline   : true,
                hoverable: true,
                popup    : '.fluid.popup',
                position : 'bottom left',
                delay: {
                    show: 300,
                    hide: 800
                }
            })
        ;

        $menuItem
            .on('click', handler.activate)
        ;

    };

    semanticMenuReady();
}); //конец  ready
