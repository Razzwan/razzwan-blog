$(document).ready(function($){
    /**
     * Всплывающие подсказки
     */
    $("[data-tooltip]").mousemove(function (eventObject) {

        $data_tooltip = $(this).attr("data-tooltip");

        $("#tooltip").text($data_tooltip)
            .css({
                "top" : eventObject.pageY + 15,
                "left" : eventObject.pageX -50
            })
            .show();

    }).mouseout(function () {

        $("#tooltip").hide()
            .text("")
            .css({
                "top" : 0,
                "left" : 0
            });
    });

    /**
     * Меню без перезагрузки
     */
    $(".nav li a").click(function(e){
        e.preventDefault();
        $(".nav li").removeClass("active");
        $(this).parent().parent().parent().addClass("active");
        if($(this).parent().parent().hasClass("dropdown-menu")){
            $(this).parent().parent().parent().addClass("active");
        } else {
            $(this).parent().addClass("active");
        }
        var url = $(this).attr("href");
        if(url != '/lessons' && url != '/articles'){
            history.pushState(null, null, url); // меняет url без перезагрузки страницы
            $.ajax({
                url: url,                              // указываем URL и
                dataType : "html",                     // тип загружаемых данных
                success: function (data) { // вешаем свой обработчик на функцию success
                    if(data.length>0){
                        $("#content").html(data);
                    }
                }
            });
        }
    });


}); //конец  ready
