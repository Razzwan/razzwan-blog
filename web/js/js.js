$(document).ready(function($){
    /**
     * Всплывающие подсказки
     */
    $("[data-tooltip]").mousemove(function (eventObject) {

        $data_tooltip = $(this).attr("data-tooltip");

        $("#tooltip").text($data_tooltip)
            .css({
                "top" : eventObject.pageY + 15,
                "left" : eventObject.pageX -50
            })
            .show();

    }).mouseout(function () {

        $("#tooltip").hide()
            .text("")
            .css({
                "top" : 0,
                "left" : 0
            });
    });

    /**
     * Меню без перезагрузки
     */
    $("body").on("click", "a", function(e){
        /**
         * отменить действие по умолчанию
         */
        e.preventDefault();

        var url = $(this).attr("href"),
            title = $(this).text(),
            regV = /^http.+/;

        /**
         * Если пользователь пытается перейти по ссылке, которая ведет на эту же страницу, то ничего не делать.
         */
        if(window.location.pathname == url) return;

        /**
         * Загрузить анимацию загрузки страницы
         */
        if(url != '/lessons' && url != '/articles'){
            $("#content").html($(".loading").html());
            $(".loading_img").css({"opacity":"0"}).animate({'opacity': 1}, 1000);
        }

        $(".nav li").removeClass("active");
        if($(this).parent().parent().hasClass("dropdown-menu")){
            $(this).parent().parent().parent().addClass("active");
        } else {
            if($(this).parent()[0].tagName == 'li' || $(this).parent()[0].tagName == 'LI'){
                $(this).parent().addClass("active");
            } else {
                $(this).addClass("active");
            }
        }

        /**
         * Если пользователь кликает на внешнюю ссылку, то перейти на нее и закончить выполнение скрипта.
         */
        if(url.search(regV) != -1){
            location.href = url;
            return;
        }

        if(url != '/lessons' && url != '/articles'){
            history.pushState(null, null, url); // меняет url без перезагрузки страницы
            $.ajax({
                url: url,                              // указываем URL и
                dataType : "html",                     // тип загружаемых данных
                success: function (data) { // вешаем свой обработчик на функцию success
                    if(data.length>0){
                        //смена titile для IE
                        $().attr('title', title);
                        //смена title
                        $('title').text(title);
                        $("#content").html(data);
                    }
                }
            });
        }
    });

    //обработка клика по лайку
    $("body").on("click", 'div.like', function(e){
        e.preventDefault();
        var
            id = $(this).data('id'),
            for_count = $(this).children(".count"),
            count = parseInt(for_count.html()),
            img = $(this).children(".like_img"),
            my_like = false;
        if(img.hasClass('glyphicon-heart-empty')) {
            my_like = true;
        }
        $.get('/like', {id:id, my_like:my_like}, function(data){
            if((data["return"])==null){
                $(".bs-example-modal-sm").modal('show');
            }
            if(data["return"] == "save"){
                for_count.html(count+1);
                img.removeClass('glyphicon-heart-empty').addClass('glyphicon-heart');
            } else {
                if(data["return"] == "delete"){
                    for_count.html(count-1);
                    img.removeClass('glyphicon-heart').addClass('glyphicon-heart-empty');
                } else {
                    $(".bs-example-modal-sm").modal('show');
                }
            }

        }, "json");
    });

    tinymce.init({
        selector: "#mytextarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });

}); //конец  ready
