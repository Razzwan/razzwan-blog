<?php
/**
 * @const bool   DEVELOP  флаг рабочей среды
 */
define('DEVELOP', true);

define("LIW_WEB", realpath(__DIR__ . '/../blog') . '/'); //определяем папку проекта

require __DIR__ . '/../vendor/autoload.php';   //Файл загрузчик дополнительных модулей
//require __DIR__ . '/../vendor/liw/core/Liw.php'; //файл загрузчик фрэймворка

/**
 * Запуск приложения
 *
 * @var $app object liw\core\base\Router
 */
(new liw\core\App())->start();
