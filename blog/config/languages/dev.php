<?php
return [
    'error_no_route' => 'Нет такого маршрута: ',
    'error_no_controller' => 'Котроллер не существует: ',
    'error_no_action' => 'Метод не существует: ',
    'def_layout_not_exist' => 'Не задан layout по умолчанию в файле config.php',
];
