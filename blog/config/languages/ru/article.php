<?php
return [
    //fields
    'article_title'=> 'Название статьи: ',
    'article_text'=> 'Текст статьи:',

    //buttons
    'button_edit_article'=> 'Сохранить изменения',
    'button_delete_article'=> 'Удалить статью',
    'button_read_more'=> 'Читать далее',

    //errors
    'error_save_article' => 'Ошибка сохранения статьи',
    'error_update_article' => 'Ошибка обновления статьи',
];
