<?php
return $ways = [
    '/'              => ['Main::index'],
    '/error'         => ['Main::error'],
    '/more'          => ['Main::more'],
    '/install'       => ['Main::install'],
    '/contacts'      => ['Main::contacts'],

    '/lessons'       => ['Main::lessons'],
    '/lesson/{:i}'   => ['Main::lessons'],

    '/articles'      => ['Article::show_all'],
    '/article/{:i}'  => ['Article::one'],

    '/logout'        => ['User::logout'],
    '/user'          => ['User::index'],

    '/like'          => ['Like::add'],

];
