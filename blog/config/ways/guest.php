<?php
return $ways = [
    '/'             => ['Main::index'],
    '/{=([a-z]+)}'  => ['Main::index'],
    '/error'        => ['Main::error'],
    '/more'         => ['Main::more'],
    '/install'      => ['Main::install'],
    '/contacts'     => ['Main::contacts'],

    '/lessons'      => ['Main::lessons'],
    '/lesson/{:i}'  => ['Main::lessons'],

    '/articles'     => ['Article::show_all'],
    '/article/{:i}' => ['Article::one'],

    '/login2182008' => ['User::login'],
    '/registration' => ['User::registration'],

    '/like'         => ['Like::add'],

];
