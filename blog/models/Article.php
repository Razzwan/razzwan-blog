<?php
namespace web\models;
use liw\core\Lang;
use liw\core\Model;

/**
 *
 * Class Article
 * @package web\models
 */
class Article extends Model
{
    /**
     * @var string
     */
    public $table = 'article';

    public function rules()
    {
        return [
            'title'       => ['required'],
            'text'        => ['required'],
            'date_update' => ['required'],
            'user_id'     => ['required'],
        ];
    }

    public function labelFields()
    {
        return [
            'title'       => 'title',
            'text'        => 'text',
            'date_create' => 'date_create',
            'date_update' => 'date_update',
            'user_id'     => 'user_id' ,
        ];
    }

    /**
     * @return bool
     */
    public function createArticle()
    {
        $this->date_update = $this->date_create = time();
        $this->user_id = $_SESSION['user']['id'];
        if($this->save()){
            return true;
        } else {
            $this->error = Lang::uage('error_save_article');
            return false;
        }
    }

    /**
     * @param $id integer
     * @return bool
     */
    public function updateArticle($id)
    {
        $this->id = $id;
        $this->date_update = time();
        if($this
            ->update(['title','text', 'date_update'])
            ->where(['id'=>$id])
            ->push()
        ){
            return true;
        } else {
            $this->error = 'error update article';
            return false;
        }

    }

    /**
     * @param $id
     */
    public function findById($id)
    {
        $this
            ->select()
            ->where(['id'=>$id])
            ->get();

        $this->date_create = date("d.m.Y H:i", $this->date_create);
        $this->author      = $this->getAuthorById($this->user_id);
        $this->my_like     = $this->getLikeById($id);
        $this->like_count  = $this->getCountById($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getAuthorById($id)
    {
        $author = new User();
        $author->findById((int)$id, ['login']);
        return $author->fields['login'];
    }

    /**
     * @param $article_id
     * @return string
     */
    private function getLikeById($article_id)
    {
        $like = new Like();
        $like->findByUserId($article_id);
        if(!isset($like->fields) || empty($like->fields)){
            return "glyphicon glyphicon-heart-empty";
        }
        return "glyphicon glyphicon-heart";
    }

    /**
     * @param $article_id
     * @return string
     */
    private function getCountById($article_id)
    {
        $like = new Like();
        $like->query( "SELECT COUNT(*) AS `count` FROM `like` WHERE `article_id` = ? ", [$article_id])->get();
        if(empty($like->fields)){
            return "0";
        }
        return $like->fields['count'];
    }

    /**
     * @param $text
     * @return string
     */
    private function textToDesk($text)
    {
        return mb_substr($text, 0, 400);
    }

    public function findLast5()
    {
        $this
            ->select()
            ->order('date_create', 'DESC')
            ->limit()
            ->get();
        if(is_array($this->fields)){
            foreach($this->fields as $key => $fields){
                if(!is_array($fields)){
                    $this->fields['text'] = $this->spaceToP($this->fields['text']);
                    $this->fields['date_create'] = date("d.m.Y H:i", $this->fields['date_create']);
                    $this->fields['author'] = $this->getAuthorById($this->fields['user_id']);
                    $this->fields['my_like'] = $this->getLikeById($this->fields['id']);
                    $this->fields['like_count'] = $this->getCountById($this->fields['id']);
                    return 'one';
                    break;
                }
                foreach($fields as $field => $value){
                    if($field=='date_create'){
                        $this->fields[$key][$field] = date("d.m.Y H:i", $value);
                    }elseif($field=='user_id'){
                        $this->fields[$key]['author'] = $this->getAuthorById($value);
                    }
                }
                $this->fields[$key]['my_like'] = $this->getLikeById($this->fields[$key]['id']);
                $this->fields[$key]['like_count'] = $this->getCountById($this->fields[$key]['id']);
            }
            return 'array';
        }
        return false;
    }

}
