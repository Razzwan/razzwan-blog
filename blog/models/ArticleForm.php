<?php
/**
 * Created by PhpStorm.
 * User: r
 * Date: 26.09.15
 * Time: 2:22
 */

namespace web\models;

use liw\core\Form;

class ArticleForm extends Form
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'title'       => ['required'],
            'text'        => ['required'],
        ];
    }
}
