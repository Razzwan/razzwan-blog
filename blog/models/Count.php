<?php
namespace web\models;

use liw\core\Model;

class Count extends Model
{
    public $table = 'count';

    public function rules()
    {
        return [
            'address' => ['required', 'min'=>3],
        ];
    }

    public function add($address)
    {
        $query = "UPDATE `" . $this->table . "` SET `count` = `count` + 1 WHERE `address` = ? ";
        $this->query($query, [$address])->push();
    }
}
