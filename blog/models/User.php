<?php
namespace web\models;

use liw\core\Lang;
use liw\core\Model;
use liw\core\Liw;

/**
 * Class ModelBD
 * @package web\model
 */
class User extends Model
{
    /**
     * @var string Название таблицы в базе данных
     */
    public $table = 'user';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'id'          => ['required'],
            'login'       => ['required', 'unique', 'min'=>3],
            'hash'        => ['required'],
            'date_create' => ['required'],
            'last_visit'  => ['required'],
        ];
    }

    /**
     * Запуск сессии
     */
    private function session_start(){
        Liw::$isGuest = false;
        $_SESSION['user'] = $this->fields;
    }

    static public function session_stop(){
        Liw::$isGuest = true;
        unset($_SESSION['user']);
    }

    public function saveUser($fields){
        $this->login  = $fields['login'];
        $this->hash   = $this->hash($fields['pass']);
        $this->levels = '2';
        $this->last_visit = $this->date_create = time();
        if($this->save()){
            $this->session_start();
            return true;
        } else {
            $this->error = Lang::uage('error_user_exist');
            $this->session_stop();
            return false;
        }
    }

    private function hash($password){
        unset($this->fields['pass']);
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function verify($fields){
        $this
            ->select()
            ->where(['login' => $fields['login']])
            ->get();

        if(!empty($this->login) && password_verify($fields['pass'], $this->hash)){
            $this->last_visit = time();
            if( $this->update(['last_visit'])->where(['id'=>$this->id])->push() ){
                print_var($this->fields);
                $this->session_start();
                return $this;
            } else {
                echo 'Ошибка!';
                exit;
            }

        }
        $this->error = Lang::uage('error_verify');
        return false;
    }

    public function findById($id, $fields){
        $result =  $this
            ->select($fields)
            ->where(['id'=>$id])
            ->get();
        return $result;
    }

}
