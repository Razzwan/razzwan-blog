<?php
namespace web\models;

use liw\core\Model;

class Like extends Model
{
    /**
     * @var string
     */
    public $table = 'like';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'user_id'       => ['required'],
            'article_id'    => ['required'],
        ];
    }

    public function labelFields(){
        return [
            'user_id'     => 'User id',
            'article_id'  => 'Article id',
        ];
    }

    public function findByUserId($article_id)
    {
        if (isset($_SESSION['user']['id'])){
            $this->select()->where(['user_id'=>$_SESSION['user']['id'], 'article_id'=>$article_id])->get();
            return;
        }
        return false;

    }
}
