<?php
namespace web\controllers;

use Gregwar\Captcha\CaptchaBuilder;
use liw\core\Controller;
use liw\core\Form;
use liw\core\Lang;
use liw\core\Liw;
use liw\core\web\Session;
use web\models\LoginForm;
use web\models\User;

class UserController extends Controller
{


    public function indexAction(){
        $this->render('index');
    }

    public function registrationAction(){
        $loginForm = new Form();

        if ($loginForm->post()) {
            $user = new User;
            $user->saveUser($loginForm->fields);

            $this->redirect('/user');

        } else {
            $model = [
                'error' => $loginForm->error,
                'login' => isset($loginForm->fields['login'])?$loginForm->fields['login']: null,
                'captcha' => $this->newCaptcha()
            ];
            $this->render('registration', $model);
        }
    }

    public function loginAction(){
        $loginForm = new Form();

        if ($loginForm->post()){
            $user = new User;
            if($user->verify($loginForm->fields)){
                $this->redirect('/user');
            } else {
                $this->redirect(['user', 'registration'], [
                    'login' => isset($loginForm->fields['login']) ? $loginForm->fields['login'] : null,
                    'error' => Lang::uage('error_verify2'),
                    'captcha' => $this->newCaptcha()
                ]);
            }
        } else {
            $this->render('login', [
                'login' => isset($loginForm->fields['login']) ? $loginForm->fields['login'] : null,
                'error' => Lang::uage('error_verify3'),
            ]);
        }
    }

    public function logoutAction(){
        User::session_stop();
        $this->redirect('/');
    }

    private function newCaptcha()
    {
        $builder = new CaptchaBuilder();
        $builder->build();
        Session::set(['phrase' => $builder->getPhrase()]);
        return $builder->inline();
    }
}
