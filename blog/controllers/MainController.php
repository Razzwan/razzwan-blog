<?php
namespace web\controllers;

use liw\core\Controller;
use liw\core\web\Request;
use web\models\Count;

class MainController extends Controller
{
    public $layout;

    public function before()
    {
        if(isset($_SESSION['layout'])){
            $layout = $_SESSION['layout'];
            if(file_exists(LIW_WEB . 'views/layouts/' . $layout)){
                $this->layout = $layout;
                return;
            }
        }
        $this->layout = 'boot';
    }

    public function indexAction($address = null)
    {
        if(!Request::isAjax()) {
            if($address === null) $address = 'unknown';
            $arr = ['unknown', 'phpforum', 'demiart', 'youtube'];
            if (in_array($address, $arr)){
                $count = new Count($address);
                $count->add($address);
            }
        }
        $this->render('index');
    }

    public function contactsAction()
    {
        $this->render('contacts');
    }

    public function articlesAction()
    {
        $this->render('articles');
    }

    public function lessonsAction($part = null)
    {
        if($part === null){
            $this->render('lessons');
        } else {
            $this->render('lessons/part' . $part);
        }
    }

}
