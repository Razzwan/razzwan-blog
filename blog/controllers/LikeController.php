<?php
namespace web\controllers;

use liw\core\Controller;
use liw\core\Liw;
use liw\core\web\Request;
use web\models\Like;

class LikeController extends Controller
{
    /*public function beforeAction()
    {
        if(
            isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ){
            return;
        } else {
            $this->redirect('main', 'error', [
                'error' => 'No Ajax.',
            ]);
        }
    }*/

    public function addAction($id = null)
    {
        if(Request::isAjax()){
            if(!Liw::$isGuest && $id !== null){
                $like = new Like();
                $like->fields['user_id'] = $_SESSION['user']['id'];
                $like->fields['article_id'] = $id;
                if($like->save()!==false){
                    echo '{"return":"save"}';
                } else {
                    $like->query("DELETE FROM `like` WHERE `user_id` = ? AND `article_id` = ? ;", [$like->fields['user_id'], $id])->push();
                    echo '{"return":"delete"}';
                }
            } else {
                echo '{"return":"isGuest"}';
            }

        } else {
            echo '{"return":"isGuest"}';
        }


    }

}
