<?php
namespace web\controllers;

use liw\core\Controller;
use liw\core\Form;
use liw\core\Lang;
use web\models\Article;
use web\models\ArticleForm;

class ArticleController extends Controller
{
    public function oneAction($id)
    {
        $article = new Article();
        $article->findById($id);
        $this->render('one', $article);
    }

    public function show_allAction()
    {
        $article = new Article();
        $request = $article->findLast5();
        if($request=='array'){

            $this->render('show_all', $article->fields);

        } elseif ($request == 'one'){

            $this->render('show_all', $article->fields);

        } else {

            $this->redirect(['main', 'error'], [
                'error' => 'No articles.',
            ]);

        }
    }

    public function createAction()
    {
        $form = new ArticleForm();
        $article = new Article();

        if ($form->post()) {
            $article->fields = $form->fields;
            $article->createArticle();
            $this->redirect('/article/' . $article->fields['id']);

        } else {
            $this->render('create', [
                'title'  => isset($article->fields['title'])?$article->fields['title']:'',
                'text'  => isset($article->fields['text'])?$article->fields['text']:'',
                'date_create'  => isset($article->fields['date_create'])?$article->fields['date_create']:'',
                'user_id'  => isset($article->fields['user_id'])?$article->fields['user_id']:'',
                'error' => $article->error?:'',
            ]);
        }
    }

    public function updateAction($id = null)
    {
        if(!empty($id)){
            $articleForm = new ArticleForm();
            $article = new Article();
            if($articleForm->post()){
                $article->fields = $articleForm->fields;
                $article->date_update = time();
                $article->user_id = $_SESSION['user']['id'];
                print_var($_SESSION);
                if($article->updateArticle($id)) {
                    $this->redirect('/article/' . $article->id);
                } else {
                    throw new \Exception(Lang::uage('error_update_article'));
                }
            } else {
                $article->findById($id);
                $article->fields['error'] = $article->error?:'';
                $this->render('edit', $article->fields);
            }
        } else {
            $this->redirect('main', 'error', [
                'error' => 'No article.',
            ]);
        }
    }

    public function deleteAction($id =  null)
    {
        if(!empty($id)){
            $article = new Article();
            if($article->delete()->where(['id'=>$id])->push()){
                $this->redirect('/articles');
            } else {
                $this->redirect('main', 'error', [
                    'error' => 'Article delete error.',
                ]);
            }
        } else {
            $this->redirect('main', 'error', [
                'error' => 'No article to delete.',
            ]);
        }
    }



}
