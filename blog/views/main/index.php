<div class="index-wrapper-content">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <img class="img-circle ui little circular image" src="/img/photo.jpg" alt="Мое фото" width="140" height="140">
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


    <div class="inner cover">
        <h1 class="cover-heading">Привет!</h1>
        <p class="lead">Ты попал в мой блог. Здесь проходит обучение сайтостроению.</p>
        <p class="lead">Если тебе интересно - давай со мной! ;)</p>
        <p class="lead">
            <a href="/lesson/2" class="btn btn-lg btn-default">А давай!</a>
        </p>
    </div>

</div>
