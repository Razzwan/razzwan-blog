<?php
use liw\core\Lang;
/**
 * Во всех видах доступны переменные класса View через конструкцию $this->variable
 * @var $login string
 * @var $error string - ошибка регистрации
 */
?>

<div class = "container">
    <div class="reg">
        <h2>Вход</h2>

        <form action="/login2182008" method="post">
            <div class="form-group">
                <input
                    type="text"
                    id="login"
                    name = "login"
                    placeholder="<?=Lang::uage('label_login');?>"
                    data-tooltip="<?=Lang::uage('message_hide_login');?>"
                    value="<?=$login?>"
                    class="form-control input-center"
                    autofocus
                    >
            </div>
            <div class="form-group">
                <input
                    type="password"
                    id="pass"
                    name = "pass"
                    placeholder="<?=Lang::uage('label_pass');?>"
                    class="form-control input-center"
                    data-tooltip="<?=Lang::uage('message_hide_pass')?>"
                    >
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Запомнить меня
                </label>
            </div>
            <input type="submit" value="Войти" class="btn btn-success">
        </form>

        <div class="error_reg">
            <?=$error;?>
        </div>
    </div>

</div>
