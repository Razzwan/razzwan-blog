<?php
use liw\core\Liw;
use liw\core\widgets\Menu;

/**
 * @var $this object liw\core\base\View
 * @var $login string
 */
?>

<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="The Liw Framework. Very lite but very strong.">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?=$this->title?></title>

    <!-- All files concat with gulp -->
    <link href="/semantic/dist/semantic.min.css" rel="stylesheet">
    <link href="/css/with_semantic.css" rel="stylesheet">

    <?php if(defined("DEVELOP") && DEVELOP == true):?>
        <link href="/css/dev.css" rel="stylesheet">
    <?php endif;?>

    <link href="/css/main.css" rel="stylesheet">

    <script src="/js/jquery.min.js" type="text/javascript" ></script>
    <script src="/semantic/dist/semantic.min.js" type="text/javascript" ></script>
    <script>
        $(document)
            .ready(function() {

                // fix main menu to page on passing
                $('.main.menu').visibility({
                    type: 'fixed'
                });
                $('.overlay').visibility({
                    type: 'fixed',
                    offset: 80
                });

                // lazy load images
                $('.image').visibility({
                    type: 'image',
                    transition: 'vertical flip in',
                    duration: 500
                });

                // show dropdown on hover
                $('.main.menu  .ui.dropdown').dropdown({
                    on: 'hover'
                });
            })
        ;
    </script>

    <!-- All files concat with gulp
    <link href="/css/main.css" rel="stylesheet"> -->



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="text container">
                <div id="content">
                    <?=$this->view;?>
                </div>
            </div>


            <div class="ui borderless main menu inverted">
                <div class="ui text container">
                    <h1 class="header item"><a href="/" class="logo"><i class="world icon"></i> Razzwan blog</a></h1>
                    <div class="right menu large monitor only">

                        <a href="#" class="red item"><i class="home icon"></i> Привет!</a>
                        <a href="#" class="item"><i class="cart icon"></i> Статьи</a>
                        <a href="#" class="ui dropdown item red">
                            Уроки <i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="header">PHP</div>
                                <div class="item">
                                    <i class="dropdown icon"></i>
                                    Часть 1 .
                                    <div class="menu">
                                        <div class="item">1 урок</div>
                                        <div class="item">2 урок</div>
                                        <div class="item">3 урок</div>
                                        <div class="item">4 урок</div>
                                        <div class="item">5 урок</div>
                                        <div class="item">6 урок</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <i class="dropdown icon"></i>
                                    Часть 2 .
                                    <div class="menu">
                                        <div class="item">1 урок</div>
                                        <div class="item">2 урок</div>
                                        <div class="item">3 урок</div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                            </div>
                        </a>
                        <a href="#" class="item">Для связи</a>

                    </div>
                </div>
            </div>

            <div>lasdfasddddddasdfasdf asdf asdf asdfhas haskjhf kajshdf kjash fkljashfkjashdkfjhsdksadfkdkjd hdf hfjh kjfh askljfh askljdfakhkhfshd l lkddddddddsfdsd fsd fsd fsd fsd fsd fsd fsd fsd fsd fsd fsd fs fsd fs fsdf s dfsdf dddddgfffffffffffffffffffffffffffffddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>
            <div>lasdfasddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</div>

        </div>

    </div>

    <footer>
        <div class="inner">
            <p>Сделано при помощи <a href="http://liw-project.tk/" class="logo">LIW - framework</a> с использованием <a href="http://getbootstrap.com">bootstrap css</a>.</p>
        </div>
    </footer>

</div>


<!-- Placed at the end of the document so the pages load faster-->
<script src="/js/js.js" type="text/javascript" ></script>

<?php if (defined("DEVELOP") && DEVELOP == true): ?>
    <div id="develop_button">Time:<?=' ' . sprintf("%d", (microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"])*1000) . 'ms';?></div>
<?php endif;?>

</body>
</html>
