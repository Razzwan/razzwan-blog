<?php
use liw\core\Liw;
use liw\core\widgets\Menu;

/**
 * @var $this object liw\core\base\View
 * @var $login string
 */
?>

<!DOCTYPE html>
<html lang="<?=$this->language;?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Блог посвященный сайтостроению и другим интересным вещам.">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title><?=$this->title?></title>

    <!-- All files concat with gulp -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/with_bootstrap.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <?php if(defined("DEVELOP") && DEVELOP == true):?>
        <link href="/css/dev.css" rel="stylesheet">
    <?php endif;?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="loading">
    <div class="loading_img">
        <img src="/img/l.gif">
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="like-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            Зарегистрируйся, чтоб лайкать ;)
        </div>
    </div>
</div>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">

                <div class="inner">
                    <nav class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-menu" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand logo" href="/">
                                    <span class="glyphicon glyphicon-user"></span>
                                    <?=Liw::$config['site_name'];?>
                                </a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="collapse-menu">

                                <?=Menu::init([
                                    'options' => ['class' => 'nav navbar-nav navbar-right'],
                                    'items' => [
                                        ['url'=>'/', 'label'=>'<span class="glyphicon glyphicon-hand"></span>Привет!'],
                                        [
                                            'url' => '/articles',
                                            'label'=>'<span class="glyphicon glyphicon-list-alt"></span> Статьи <span class="caret"></span>',
                                            'options'=>[
                                                'class'         => 'dropdown-toggle my-drop',
                                                'data-toggle'   => 'dropdown',
                                                'role'          => 'button',
                                                'aria-haspopup' => 'true',
                                                'aria-expanded' => 'false',
                                            ],
                                            'items' => [
                                                ['url'=>'/articles?', 'label'=>'PHP'],
                                                ['url'=>'/article/1', 'label'=>'Ubuntu'],
                                                ['url'=>'/articles', 'label'=>'Git'],
                                                ['url'=>'/articles', 'label'=>'Bootstrap css'],
                                                ['url'=>'/articles', 'label'=>'Composer и др.'],
                                            ],
                                        ],
                                        [
                                            'url' => '/lessons',
                                            'label'=>'<span class="glyphicon glyphicon-facetime-video"></span> Уроки <span class="caret"></span>',
                                            'options'=>[
                                                'class'         => 'dropdown-toggle my-drop',
                                                'data-toggle'   => 'dropdown',
                                                'role'          => 'button',
                                                'aria-haspopup' => 'true',
                                                'aria-expanded' => 'false',
                                            ],
                                            'items' => [
                                                ['url'=>'/lesson/1', 'label'=>'PHP. ч-I'],
                                                ['url'=>'/lesson/2', 'label'=>'PHP. ч-II'],
                                            ],
                                        ],
                                        ['url'=>'/contacts', 'label'=>'Для связи'],
                                        Liw::$isGuest?null:['url'=>'/user', 'label'=>$_SESSION['user']['login']],
                                        Liw::$isGuest?null:['url'=>'/logout', 'label'=>'Выход'],
                                    ]
                                ]);?>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div>
            </div>

            <div class="container">
                <div id="content">
                    <?=$this->view;?>
                </div>
            </div>

        </div>

    </div>

    <footer>
        <div class="inner">
            <p>Сделано при помощи <a href="http://liw-project.tk/" class="logo">LIW - framework</a> с использованием <a href="http://getbootstrap.com">bootstrap css</a>.</p>
        </div>
    </footer>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster-->
<script src="/js/jquery.min.js" type="text/javascript" ></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/tinymce/tinymce.min.js"></script>
<script src="/js/js.js" type="text/javascript" ></script>
<?php if (defined("DEVELOP") && DEVELOP == true): ?>
    <script src="/js/dev.js" type="text/javascript" ></script>
    <div id="develop_button">Time:<?=' ' . sprintf("%d", (microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"])*1000) . 'ms';?></div>
<?php endif;?>
</body>
</html>
