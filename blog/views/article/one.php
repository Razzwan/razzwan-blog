<?php
/**
 * @var $this - liw\core\base\View
 */
?>
<article>
    <?php $this->showBlock('levels/edit_article');?>
    <?php $this->showBlock('_field');?>
</article>
