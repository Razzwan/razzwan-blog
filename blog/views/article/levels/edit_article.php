<?php
use liw\core\Lang;
use liw\core\Liw;

if(Liw::level('article')>=2):?>

    <a href="/delete_article?id=<?=$id;?>" id="delete" data-tooltip="<?=Lang::uage('button_delete_article');?>">
        <span class="glyphicon glyphicon-remove-sign"></span>
    </a>

    <a href="/edit_article?id=<?=$id;?>" id="edit" data-tooltip="<?=Lang::uage('button_edit_article');?>">
        <span class="glyphicon glyphicon-edit"></span>
    </a>

<?php endif;?>
