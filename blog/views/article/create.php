<?php
use liw\core\Liw;

/**
 * @var $title string - �������� ������
 * @var $text string - ����� ������
 * @var $error string - ����� ��������� ������
 */
?>
<article>

    <form action="/add_article" method="post">
        <table>
            <tr>
                <td class="text_right"><?=Lang::uage('label_title_article');?></td>
                <td colspan="2"><input type="text" name="title" value="<?=$title;?>"></td>
            </tr>

            <tr>
                <td class="padding_left_40px"><?=Lang::uage('label_text_article');?></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3"><textarea name="text"><?=$text;?></textarea></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" value="<?=Lang::uage('button_add_article');?>" class="button"></td>
            </tr>
        </table>

    </form>

    <div class="error_article">
        <?=$error;?>
    </div>

</article>


