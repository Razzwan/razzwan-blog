<?php
/**
 * @var $id integer
 * @var $title string
 * @var $text string
 * @var $date_create string date
 * @var $author string
 * @var $like_count int
 * @var $my_like string style
 */
?>

<h1><?=$title;?></h1>

<?=$text;?>

<table id="article_footer">
    <tr>
        <td class="date"><?=$date_create;?></td>
        <td class="author">&copy<?=$author;?></td>
    </tr>
</table>

<div class="like" data-id="<?=$id;?>">
    <span class="like_img <?=$my_like;?>" ></span>
    <span class="count"><?=$like_count?:'0';?></span>
</div>
