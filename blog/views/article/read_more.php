<?php
use liw\core\Lang;
/**
 * @var $id
 */
?>
<form action="/article/<?=$id;?>" method="post" id="read_more"">
    <input type="submit" class="button" value="<?=Lang::uage('button_read_more');?>" data-tooltip="<?=Lang::uage('button_read_more');?>">
</form>
