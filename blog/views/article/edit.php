<?php
use liw\core\Lang;

/**
 * @var $id int
 * @var $title string
 * @var $text string
 * @var $error string
 */
?>
<article>

    <form action="/edit_article?id=<?=$id;?>" method="post">
        <table>
            <tr>
                <td class="text_right"><?=Lang::uage('article_title');?></td>
                <td colspan="2"><input type="text" name="title" class="input" value="<?=$title;?>"></td>
            </tr>

            <tr>
                <td class="padding_left_40px"><?=Lang::uage('article_text');?></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3"><textarea id="mytextarea" name="text"><?=$text;?></textarea></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" value="<?=Lang::uage('button_edit_article');?>" class="button"></td>
            </tr>
        </table>

    </form>

    <div class="error_article">
        <?=$error;?>
    </div>

</article>


