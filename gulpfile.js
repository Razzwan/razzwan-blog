var browserSync = require('browser-sync').create(),
    gulp = require('gulp'),
    concatCss = require('gulp-concat-css'),
    minifyCss = require('gulp-minify-css'),
    less = require('gulp-less')
    ;

gulp.task('connect', function() {
    browserSync.init({
        proxy: 'blog.loc/'
    });

    gulp.watch('web/assets/my/*.less', ['my_styles']);

    gulp.watch('web/assets/bootstrap/*.less', ['bootstrap']);

    gulp.watch('web/assets/semantic/*.less', ['semantic']);

    gulp.watch('blog/views/**/*.php').on('change', browserSync.reload);

    gulp.watch('blog/config/languages/**/*.php').on('change', browserSync.reload);

    gulp.watch('web/js/**/*.js').on('change', browserSync.reload);
});

// my_styles
gulp.task('my_styles', function(){
    gulp.src('web/assets/my/*.less')
        .pipe(less())
        .pipe(concatCss("css/main.css"))
        .pipe(minifyCss())
        .pipe(gulp.dest('web/'))
        .pipe(browserSync.stream());
});

// bootstrap
gulp.task('bootstrap', function(){
    gulp.src('web/assets/bootstrap/*.less')
        .pipe(less())
        .pipe(concatCss("css/with_bootstrap.css"))
        .pipe(minifyCss())
        .pipe(gulp.dest('web/'))
        .pipe(browserSync.stream());
});

// semantic
gulp.task('semantic', function(){
    gulp.src('web/assets/semantic/*.less')
        .pipe(less())
        .pipe(concatCss("css/with_semantic.css"))
        .pipe(minifyCss())
        .pipe(gulp.dest('web/'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['connect']);
